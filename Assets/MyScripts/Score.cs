﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	// Variables for current and best score
	public static int scoreNum;
	public static int bestNum;
	public AudioClip ClearClip;
	public static AudioSource ClearSource;
	private bool changeFlag = false;

	[SerializeField]
	Sprite AF_KANBAN;

	[SerializeField]
	Sprite KanBan;

	[SerializeField]
	GameObject af_GOAL;
	// Use this for initialization
	void Start () {

		// Starting score is 0
     /*   if (gameObject.name != "GameOverScoreUI")
        {
            scoreNum = 0;
        }*/

		ClearSource = gameObject.AddComponent<AudioSource>();
		ClearSource.playOnAwake = false;
		ClearSource.volume = 2f;
		ClearSource.rolloffMode = AudioRolloffMode.Linear;
		ClearSource.clip = ClearClip;
		
		// Get previously saved best score and set to the bestNum variable
		bestNum = PlayerPrefs.GetInt("Score", 0);

	}
	
	// Update is called once per frame
	void Update () {
	
		// Set/ Update the score text.
        
   //     gameObject.GetComponent<Text>().text = "Score: " + scoreNum;
        
         
	}

	public void FinishScore(){
		if (scoreNum >= 10000 && !changeFlag) {
			this.GetComponent<Image>().sprite = AF_KANBAN;
			af_GOAL.SetActive(true);
			ClearSource.Play();
		//	Debug.Log("OK");
			changeFlag = true;
		}
	}
}
