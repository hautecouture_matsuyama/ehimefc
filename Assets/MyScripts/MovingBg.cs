﻿using UnityEngine;
using System.Collections;

public class MovingBg : MonoBehaviour {

	public GameObject tilePrefab;
	
	private float bottomY;
	private float leftX;
	private float topY;
	private float rightX;
	
	private float groundTileW;
	private float groundTileH;
	
	private GameObject block1;
	private GameObject block2;
	private GameObject block3;
	private GameObject block4;
	private GameObject block5;
	
	// This code is similar to the Clouds Code. Refer that code for detail explanation of lines.
	
	// Use this for initialization
	void Start () {
		float dist = (transform.position - Camera.main.transform.position).z;
		
		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;
		
		groundTileW = tilePrefab.GetComponent<Renderer>().bounds.size.x;
		//groundTileH = tilePrefab.renderer.bounds.size.y;
		groundTileH = 0.8f;
		
		block1 = Instantiate(tilePrefab, new Vector3(leftX, bottomY+ groundTileH, 0), Quaternion.identity) as GameObject;
		block2 = Instantiate(tilePrefab, new Vector3(block1.transform.position.x+ groundTileW -0.01f, bottomY+ groundTileH, 0), Quaternion.identity) as GameObject;
		block3 = Instantiate(tilePrefab, new Vector3(block2.transform.position.x+ groundTileW -0.01f, bottomY+ groundTileH, 0), Quaternion.identity) as GameObject;
		block4 = Instantiate(tilePrefab, new Vector3(block3.transform.position.x+ groundTileW -0.01f, bottomY+ groundTileH, 0), Quaternion.identity) as GameObject;
		block5 = Instantiate(tilePrefab, new Vector3(block4.transform.position.x+ groundTileW -0.01f, bottomY+ groundTileH, 0), Quaternion.identity) as GameObject;
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 moveDir = new Vector3(1, 0, 0);

		block1.transform.position = new Vector3(block1.transform.position.x, Ground.posY + Ground.groundH/2 + groundTileH/2f, block1.transform.position.z);
		block2.transform.position = new Vector3(block2.transform.position.x, Ground.posY + Ground.groundH/2 + groundTileH/2f, block2.transform.position.z);
		block3.transform.position = new Vector3(block3.transform.position.x, Ground.posY + Ground.groundH/2 + groundTileH/2f, block3.transform.position.z);
		block4.transform.position = new Vector3(block4.transform.position.x, Ground.posY + Ground.groundH/2 + groundTileH/2f, block4.transform.position.z);
		block5.transform.position = new Vector3(block5.transform.position.x, Ground.posY + Ground.groundH/2 + groundTileH/2f, block5.transform.position.z);

		block1.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		block2.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		block3.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		block4.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		block5.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		
		if(block1.transform.position.x+ groundTileW < leftX){			
			block1.transform.position= new Vector3(block5.transform.position.x+ groundTileW -0.01f, block1.transform.position.y, 0);
		}
		if(block2.transform.position.x+ groundTileW < leftX){			
			block2.transform.position= new Vector3(block1.transform.position.x+ groundTileW -0.01f, block2.transform.position.y, 0);
		}
		if(block3.transform.position.x+ groundTileW < leftX){			
			block3.transform.position= new Vector3(block2.transform.position.x+ groundTileW -0.01f, block3.transform.position.y, 0);
		}
		if(block4.transform.position.x+ groundTileW < leftX){			
			block4.transform.position= new Vector3(block3.transform.position.x+ groundTileW -0.01f, block4.transform.position.y, 0);
		}
		if(block5.transform.position.x+ groundTileW < leftX){			
			block5.transform.position= new Vector3(block4.transform.position.x+ groundTileW -0.01f, block5.transform.position.y, 0);
		}
	}
}
