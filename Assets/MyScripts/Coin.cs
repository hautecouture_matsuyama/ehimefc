﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	// Boolean variables to check if coin is in collision with Player and Obstacles
	private bool inTrain;
	private bool collided;
	private bool inObsUp;

	// Coind pickup sound
	public AudioClip coinPickClip;
	private AudioSource coinPickSource;

	// Coin side check transform, this is in the coin prefab
	private Transform sideCheck;

	void Start () {
		// Get side check 
		sideCheck = transform.Find("SideCheck");

		// Set Coin Pickup Sound
		coinPickSource = gameObject.AddComponent<AudioSource>();
		coinPickSource.playOnAwake = false;
		coinPickSource.rolloffMode = AudioRolloffMode.Linear;
		coinPickSource.clip = coinPickClip;
	}
	
	// Update is called once per frame
	void Update () {

		// Check if Coin is collided with player and obstacles
		inTrain =  Physics2D.Linecast(transform.position, sideCheck.position, 1 << LayerMask.NameToLayer("Ground"));  
		collided =  Physics2D.Linecast(transform.position, sideCheck.position, 1 << LayerMask.NameToLayer("Player"));  
		inObsUp =  Physics2D.Linecast(transform.position, sideCheck.position, 1 << LayerMask.NameToLayer("ObsUp"));  

		// If coin is in collision with Train/Car, we move coin a few units UP
		if(inTrain){
			transform.position = new Vector3(transform.position.x, transform.position.y+ 1, transform.position.z);
		}

		// If coin is in collision with Roadblock obstacle, we move coin a few units DOWN
		if(inObsUp){
			transform.position = new Vector3(transform.position.x, transform.position.y- 0.8f, transform.position.z);
		}

		// If coin collides with Player, we remove coin from screen,
		// Play coin pickup sound, increase score value
		if(collided){
			transform.position = new Vector3(transform.position.x- 25, transform.position.y, transform.position.z);
			coinPickSource.Play();
			Score.scoreNum += 1;
		}
	}
}


