﻿using UnityEngine;
using System.Collections;

public class ButtonPause : MonoBehaviour {

	public Color colorPushed = new Color(0.66f,0.66f,0.48f);   
	private Color originalColor;
    public static bool onMouseFlg;
	ButtonEffect btnResume,btnHome,btnReplay;
	bool EffctFlag = false;//アップデートで一回だけ実行
	PlayerControl plControler; //プレイヤーController

	// Use this for initialization
	void Start () {
        onMouseFlg = false;
		//originalColor = gameObject.renderer.material.color;   
		plControler = GameObject.Find ("Player").GetComponent<PlayerControl> ();
		btnResume = GameObject.Find ("btn-resume").GetComponent<ButtonEffect>();
		btnHome = GameObject.Find("btn-home").GetComponent<ButtonEffect>();;
		btnReplay = GameObject.Find ("btn-replay").GetComponent<ButtonEffect>();;
	}
	
	// Update is called once per frame
	void Update () {
		/*if (GameScreen.paused == true) {
			if (!EffctFlag) {
				btnResume.PunchEffect ();
				btnHome.PunchEffect ();
				btnReplay.PunchEffect ();
				EffctFlag = true;
			}
		} else {
			EffctFlag = false;
		}*/
	}

    void OnMouseEnter()
    {
        onMouseFlg = true;
    }

	void OnMouseExit() {
        onMouseFlg = false;
		gameObject.GetComponent<Renderer>().material.color= originalColor;
	}

	void OnMouseDown() {
		gameObject.GetComponent<Renderer>().material.color= colorPushed;
	}
	
 public	void OnMouseUpAsButton() {   

		Time.timeScale = 0; // pause
		GameScreen.paused = true;
		// Make paused screen elements visible when paused
		GameObject.Find("Pause-bg").GetComponent<Renderer>().enabled = true;
		GameObject.Find("btn-resume").GetComponent<Renderer>().enabled = true;

		GameObject.Find("btn-home").GetComponent<Renderer>().enabled = true;

		GameObject.Find("btn-replay").GetComponent<Renderer>().enabled = true;

//		GameObject.Find("Paused Text").guiText.enabled = true;
		plControler.MidstreamStop ();
		PlayerControl.musicSource.Stop();
		GameObject.Find ("btn-pause").SetActive (false);
		GameObject.Find("PausedObs").GetComponent<Renderer>().enabled = true;

	}

}
