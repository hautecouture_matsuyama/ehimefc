﻿using UnityEngine;
using System.Collections;

public class ButtonPlay : MonoBehaviour {

	public Color colorPushed  = new Color(0.66f,0.66f,0.48f);   
	private Color originalColor;
	bool Flag = false;
	float timer=0;

	public  AudioSource musicSource;
	public AudioClip musicClip;

	// Use this for initialization
	void Start () {
	//	originalColor = gameObject.renderer.material.color;   
		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.playOnAwake = false;
		musicSource.volume = 0.75f;
		musicSource.loop = true;
		musicSource.rolloffMode = AudioRolloffMode.Linear;
		musicSource.clip = musicClip;
	}
	
	// Update is called once per frame
	void Update () {

	}  

	IEnumerator Sample() {
		yield return new WaitForSeconds (1.5f);
		Application.LoadLevel (1);

	}

	void OnMouseExit() {
		gameObject.GetComponent<Renderer>().material.color= originalColor;
	}
	
	void OnMouseDown() {
		gameObject.GetComponent<Renderer>().material.color= colorPushed;
	}
	
public void OnMouseUpAsButton() {
		if (MenuScreen.isSoundOn) {
			musicSource.PlayOneShot (musicClip);
		}
		StartCoroutine ("Sample");
	}

}


