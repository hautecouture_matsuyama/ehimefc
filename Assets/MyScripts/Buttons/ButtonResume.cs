﻿using UnityEngine;
using System.Collections;

public class ButtonResume : MonoBehaviour {

	// Use this for initialization
	public Color colorPushed  = new Color(0.66f,0.66f,0.48f);   
	private Color originalColor;
	PlayerControl plControler; //プレイヤーController

	[SerializeField]
	GameObject btnPause;
	
	// Use this for initialization
	void Start () {
		originalColor = gameObject.GetComponent<Renderer>().material.color;    
		plControler = GameObject.Find ("Player").GetComponent<PlayerControl> ();
	}
	
	// Update is called once per frame
	void Update () {
		
        if(!GameScreen.paused)
        {
            this.GetComponent<Collider2D>().enabled = false;
        }
        else
        {
            this.GetComponent<Collider2D>().enabled = true;
        }
	}  
	
	void OnMouseExit() {
		gameObject.GetComponent<Renderer>().material.color= originalColor;
	}
	
	void OnMouseDown() {
		gameObject.GetComponent<Renderer>().material.color= colorPushed;
	}
	
	void OnMouseUpAsButton() {     

		if(Time.timeScale != 1 && GameScreen.paused){
			GameObject.Find("Pause-bg").GetComponent<Renderer>().enabled = false;
			GameObject.Find("btn-resume").GetComponent<Renderer>().enabled = false;
			GameObject.Find("btn-home").GetComponent<Renderer>().enabled = false;
			GameObject.Find("btn-replay").GetComponent<Renderer>().enabled = false;
		//	GameObject.Find("Paused Text").guiText.enabled = false;
			btnPause.SetActive(true);
			GameObject.Find("PausedObs").GetComponent<Renderer>().enabled = false;

			if(MenuScreen.isSoundOn){
				plControler.MidstreamPlay();
				PlayerControl.musicSource.Play();
			}
			Time.timeScale = 1;
			GameScreen.paused = false;

		}
	
	}
}
