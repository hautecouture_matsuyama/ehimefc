﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
using NendUnityPlugin.Common;

public class ButtonReplayOver : MonoBehaviour {

	// Use this for initialization
	public Color colorPushed  = new Color(0.66f,0.66f,0.48f);   
	private Color originalColor;
	private GameObject banner;
	
	// Use this for initialization
	void Start () {
		originalColor = gameObject.GetComponent<Renderer>().material.color;    
		banner = GameObject.Find ("nendAdBanner");
	}
	
	// Update is called once per frame
	void Update () {
		if(!PlayerControl.isDead)
        {
            this.GetComponent<Collider2D>().enabled = false;
        }
        else
        {
            this.GetComponent<Collider2D>().enabled = true;
        }
	}  
	
	void OnMouseExit() {
		gameObject.GetComponent<Renderer>().material.color= originalColor;
	}
	
	void OnMouseDown() {
		gameObject.GetComponent<Renderer>().material.color= colorPushed;
	}
	
	void OnMouseUpAsButton() {       
		if(Time.timeScale != 1 && PlayerControl.isDead){
			Time.timeScale = 1;
			Application.LoadLevel(1);
        //   NendUtils.GetBannerComponent("nendAdBanner").Destroy();
		}
	}
}
