﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour {

	// Use this for initialization
    public Color colorPushed;   
	private Color originalColor;

	[SerializeField]
	private GameObject buttonOff;
	[SerializeField]
	private GameObject buttonOn;

	[SerializeField]
	GameObject TitleBGMObs;
	

	// Use this for initialization
	void Start () {

		//originalColor = gameObject.renderer.material.color;    

		// Get Screen border variables
		float dist = (transform.position - Camera.main.transform.position).z;
		float leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		float rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		float topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		float bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;

		//transform.position = new Vector3(rightX - transform.renderer.bounds.size.x, topY - transform.renderer.bounds.size.y, 0);

		buttonOff.transform.position = transform.position;

		if(MenuScreen.isSoundOn){
		//	gameObject.renderer.enabled = true;
			//buttonOff.gameObject.renderer.enabled = false;
		}else{
		//	buttonOff.gameObject.renderer.enabled = true;
		//	gameObject.renderer.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	 if (MenuScreen.isSoundOn) {
			buttonOn.SetActive (true);
			buttonOff.SetActive (false);

		} else {
			buttonOff.SetActive(true);
			buttonOn.SetActive(false);
		}
	}

    //void OnMouseExit() {

    //    //gameObject.renderer.material.color= originalColor;
    //}
	
    //void OnMouseDown() {
    //    //gameObject.renderer.material.color= colorPushed;
    //}
	
	public void OnMouseUpAsButton() {
		if(MenuScreen.isSoundOn){
			MenuScreen.isSoundOn = false;
			TitleBGMObs.GetComponent<TitleBGM>().musicSource.Stop();
			buttonOff.SetActive(true);
			buttonOn.SetActive(false);


		}
	}
	public void OnMouseSoundOff(){

		if(!MenuScreen.isSoundOn){
			MenuScreen.isSoundOn = true;
			TitleBGMObs.GetComponent<TitleBGM>().musicSource.Play();
			buttonOn.SetActive(true);
			buttonOff.SetActive(false);
		}
	}

}
