﻿using UnityEngine;
using System.Collections;

public class ButtonHome : MonoBehaviour {

	
	// Use this for initialization
	public Color colorPushed  = new Color(0.66f,0.66f,0.48f);   
	private Color originalColor;
	
	// Use this for initialization
	void Start () {
		originalColor = gameObject.GetComponent<Renderer>().material.color;    
	}
	
	// Update is called once per frame
	void Update () {
        if (!GameScreen.paused)
        {
            this.GetComponent<Collider2D>().enabled = false;
        }
        else
        {
            this.GetComponent<Collider2D>().enabled = true;
        }
	}  
	
	void OnMouseExit() {
		gameObject.GetComponent<Renderer>().material.color= originalColor;
	}
	
	void OnMouseDown() {
		gameObject.GetComponent<Renderer>().material.color= colorPushed;
	}
	
	void OnMouseUpAsButton() {

		if(Time.timeScale != 1 && GameScreen.paused){
			Time.timeScale = 1;
			Application.LoadLevel(0);

		}
	}
}
