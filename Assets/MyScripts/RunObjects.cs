﻿using UnityEngine;
using System.Collections;

// When applied in any GameObject, it will move according to speed given in PlayerControl class
public class RunObjects : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 moveDir = new Vector3(1f, 0, 0);
		transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
	}
}
