﻿using UnityEngine;
using System.Collections;

public class MovingBall : MonoBehaviour {


    private float sign;
    private int nowTime;
    private const short moveRimit = 30;
    public short rotateSpeed;
	// Use this for initialization
	void Start () {

        GameObject player = GameObject.Find("Player");
        sign = -1;
        rotateSpeed = -800;
        nowTime = 0;

        Vector3 position = new Vector3(player.transform.position.x + (player.GetComponent<Renderer>().bounds.size.x / 3) + gameObject.GetComponent<Renderer>().bounds.size.x,
            0,0);

        transform.position = position;
	}
	
	// Update is called once per frame
	void Update () {

        if (!GameScreen.paused && !PlayerControl.isDead)
        {
            Vector3 position = transform.position;

            transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime));
            transform.position = new Vector3(position.x + (gameObject.GetComponent<Renderer>().bounds.size.x * 2 * sign * Time.deltaTime), position.y, 0.0f);
            if (nowTime++ > moveRimit)
            {
                sign *= -1;
                nowTime = 0;
            }
        }
	}
}
