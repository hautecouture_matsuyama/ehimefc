﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SherePlugins : MonoBehaviour {

    public Texture2D shareTexture;

    public void ShareTwitter()
    {

        int score = Score.scoreNum;
		string url = "https://play.google.com/store/apps/details?id=com.hautecouture.AnimalFantasista&hl=ja";
		//UM_ShareUtility.TwitterShare("Animal Fantasista! " + score + " まで進んだよ！キミはどこまでドリブルできる？目指せ！ファンタジスタ！" + url, shareTexture);
		//Debug.Log ("Pushed!");
    }

    public void ShareLINE()
    {
        int score = Score.scoreNum;
		string msg = "Animal Fantasista! " + score + " まで進んだよ！キミはどこまでドリブルできる？目指せ！ファンタジスタ！" + "https://play.google.com/store/apps/details?id=com.hautecouture.AnimalFantasista&hl=ja";
        string url = "http://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
    }

    public void OnClick()
    {
        if (gameObject.name == "share_facebook")
        {
            ShareFacebook();
        }

        if (gameObject.name == "share_line")
        {
            ShareLINE();
        }

        if (gameObject.name == "share_twitter")
        {
            ShareTwitter();
        }
    }
}