﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {

	public GameObject tilePrefab;

	private float bottomY;
	private float leftX;
	private float topY;
	private float rightX;
	
	private float groundTileW;
	private float groundTileH;

	private GameObject block1;
	private GameObject block2;

	public static float posY = 0;
	public static float groundH = 0;


	// This code is similar to the Clouds Code. Refer that code for detail explanation of lines.

	// Use this for initialization
	void Start () {
        //
		float dist = (transform.position - Camera.main.transform.position).z;
		
        //画面内の端の座標をそれぞれ算出する
		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;
        //地面の画像幅のサイズを取得する
		groundTileW = tilePrefab.GetComponent<Renderer>().bounds.size.x;
		groundTileH = tilePrefab.GetComponent<Renderer>().bounds.size.y;

		block1 = Instantiate(tilePrefab, new Vector3(leftX, bottomY + groundTileH/1.8f, 0), Quaternion.identity) as GameObject;
		block2 = Instantiate(tilePrefab, new Vector3(block1.transform.position.x+ groundTileW, bottomY + groundTileH/1.8f, 0), Quaternion.identity) as GameObject;

		posY = block1.transform.position.y;
		groundH = groundTileH;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 moveDir = new Vector3(1, 0, 0);

		block1.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);
		block2.transform.Translate(moveDir * PlayerControl.speed * Time.deltaTime);

        //画面外に出たら、ループさせるようにもう一つの画像の後ろに持ってくる
		if(block1.transform.position.x+ groundTileW/2 < leftX){			
			block1.transform.position= new Vector3(block2.transform.position.x+ groundTileW, block1.transform.position.y, 0);
		}
		if(block2.transform.position.x+ groundTileW/2 < leftX){			
			block2.transform.position= new Vector3(block1.transform.position.x+ groundTileW, block2.transform.position.y, 0);
		}

	}
}





