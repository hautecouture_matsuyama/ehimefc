﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class MenuScreen : MonoBehaviour {


	// Screen border variables
	private float bottomY;
	private float leftX;
	private float topY;
	private float rightX;

	// Ground variable and its height
	public GameObject groundPrefab;
	private float groundTileH;

	public static bool isSoundOn = true;

	// Use this for initialization
	void Start () {

		// Speed is 0 in menu screen
		// Get Screen border variables
		float dist = (transform.position - Camera.main.transform.position).z;
		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;

		// Get player and obstacles from scene
		//GameObject player= GameObject.Find("player");
		//GameObject obsBelow= GameObject.Find("obsBelow");
		//GameObject obsAbove= GameObject.Find("obsAbove");
        
		// Ground height
		groundTileH = groundPrefab.GetComponent<Renderer>().bounds.size.y;

		// Set position of player and obstacles relative to player
		//player.transform.position = new Vector3(player.transform.position.x, bottomY + groundTileH/2 + player.transform.renderer.bounds.size.y/2.4f, 0);
		//obsBelow.transform.position = new Vector3(obsBelow.transform.position.x, bottomY + groundTileH/2 + obsBelow.transform.renderer.bounds.size.y/2, 0);
		//obsAbove.transform.position = new Vector3(obsAbove.transform.position.x, bottomY + groundTileH/2 + player.transform.renderer.bounds.size.y/2 + obsAbove.transform.renderer.bounds.size.y, 0);


	}
	
	// Update is called once per frame
	void Update () {
		// When back button is pressed in menu screen, quit game
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}
	}
}




