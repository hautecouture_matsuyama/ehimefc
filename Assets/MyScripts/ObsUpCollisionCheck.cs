﻿using UnityEngine;
using System.Collections;

public class ObsUpCollisionCheck : MonoBehaviour {
	
	//private bool collided= false; // Boolean to check if collided with player
	private bool isDead = false;  // Boolean to check if player is dead
	//private Transform sideCheck;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	// Triggered when this object is about to pass through / collided with other objects
	void OnTriggerStay2D(Collider2D c)
	{
		// If collided with player and player is not sliding,
		// player should be dead
		if(c.gameObject.tag == "Player" && !PlayerControl.slide && !isDead){
			if(MenuScreen.isSoundOn){
				GameScreen.deadSource.Play(); // Play dead sound
			}
			PlayerControl.anim.SetTrigger("Die"); // Set dead animation, in this case explision
			PlayerControl.speed = 0;
			isDead = true;
			PlayerControl.isDead = true;

		}
	}
	

}
