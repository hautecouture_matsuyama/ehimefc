﻿using UnityEngine;
using System.Collections;

public class ObsDownCollisionCheck : MonoBehaviour {
	// Boolean variables to check collisions
	//private bool collided= false;
	private bool isDead = false;
	
	// Transform for side of the obstacles
	//private Transform sideCheck;
	
	
	void Awake() {
		
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerStay2D(Collider2D c)
	{
		// If collided with player and player is not sliding,
		// player should be dead
		if(c.gameObject.tag == "Player" && !isDead){
			if(MenuScreen.isSoundOn){
				GameScreen.deadSource.Play(); // Play dead sound
			}
			PlayerControl.anim.SetTrigger("Die"); // Set dead animation, in this case explision
			PlayerControl.speed = 0;
			isDead = true;
			PlayerControl.isDead = true;
		}
	}

}





