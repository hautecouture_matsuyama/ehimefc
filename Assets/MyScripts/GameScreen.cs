﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
using NendUnityPlugin.Common;


public class GameScreen : MonoBehaviour {

    private string platformType = null;
	// Ground prefab variable
	public GameObject groundPrefab;

	public Score scoreData;

	[SerializeField]
	GameObject[] topObjs = new GameObject[1];

	[SerializeField]
	GameObject[] bottomObjs = new GameObject[1];

	[SerializeField]
	GameObject overScoreObs;//ゲームオーバーテクスチャ
	
	// Obstacle Prefabs for Low and High obstacles.
	public GameObject obsPrefab;
	public GameObject obs2Prefab;

	// Obstacles array variable.
	private GameObject[] obs;

	// Player variable
	private GameObject player;

	// Screen border variables
	private float bottomY;
	private float leftX;
	private float topY;
	private float rightX;

	// Height variale of obstacle and ground
	private float obsTileH;
	private float obs2TileH;
	private float groundTileH;

	// Boolean variable to determine is game is paused
	public static bool paused;

	// This variable will determine position of obstacles either low or high
	private float obsHeightScale;

	// Gap between two obstacles. This decreases over time to increase difficulty
	private float gap;

	private float height1, height2;
	private BoxCollider2D obsCollider, obs2Collider;

	// AdMob variable
//	public static AdMobPlugin admob;

	[SerializeField]
	GameObject[] NUM = new GameObject[1];

	bool ScoreNumMoveFlag = false;//字幅詰めフラグ
	bool BestNumMoveFlag = false;//字幅詰めフラグ

	// Game over sound
	public AudioClip overClip;
	private AudioSource overSource;
	public AudioClip deadClip;
	public static AudioSource deadSource;

	public static bool canControl = true;

    private int objectMax = 6;


    private Renderer pauseBg;
    private Renderer btnResume;
    private Renderer btnHome;
    private Renderer btnReplay;
    private GUIText pauseText;
    private Renderer overBg;
    private Renderer btnHomeOver;
    private Renderer btnReplayOver;
    private GameObject btnPause;

    private NendAdBanner banner;

    private GameObject SherePlugins;


    void Awake()
    {
        SherePlugins = GameObject.Find("Canvas");
        SherePlugins.SetActive(false);
    }

	// Use this for initialization
	void Start () {

        banner = null;

#if UNITY_IPHONE
        NendAdInterstitial.Instance.Load("6422ed0ee181c9a3f8b5768cde25f659d1b99477", "372129");
#elif UNITY_ANDROID
        NendAdInterstitial.Instance.Load("db14fb355ef7711e27258ab259a592501ec91e3f", "372136");
#endif


		// Starting game speed
		PlayerControl.speed = -2.8f;

		// Game is not paused at start
		paused = false;

		// Hide Paused ang Game Over Screen elements at start
		pauseBg = GameObject.Find("Pause-bg").GetComponent<Renderer>();
        pauseBg.enabled = false;
        
        btnResume = GameObject.Find("btn-resume").GetComponent<Renderer>();
        btnResume.enabled = false;

        btnHome = GameObject.Find("btn-home").GetComponent<Renderer>();
        btnHome.enabled = false;


        btnReplay = GameObject.Find("btn-replay").GetComponent<Renderer>();
        btnReplay.enabled = false;

		//画像表示のためコメントアウト（テキストの場合コメント解除
        /*pauseText = GameObject.Find("Paused Text").guiText;
        pauseText.enabled = false;*/

        overBg = GameObject.Find("Over-bg").GetComponent<Renderer>();
        overBg.enabled = false;

        btnReplayOver = GameObject.Find("btn-replay-over").GetComponent<Renderer>();
        btnReplayOver.enabled = false;

        btnHomeOver = GameObject.Find("btn-home-over").GetComponent<Renderer>();
        btnHomeOver.enabled = false;

        btnPause = GameObject.Find("btn-pause");
		btnPause.SetActive (true);
      

        //GameObject.Find("Pause-bg").renderer.enabled = false;
        //GameObject.Find("btn-resume").renderer.enabled = false;
        //GameObject.Find("btn-home").renderer.enabled = false;
        //GameObject.Find("btn-replay").renderer.enabled = false;
        //GameObject.Find("Paused Text").guiText.enabled = false;

        //GameObject.Find("Over-bg").renderer.enabled = false;
        //GameObject.Find("btn-home-over").renderer.enabled = false;
        //GameObject.Find("btn-replay-over").renderer.enabled = false;
        //GameObject.Find("Game Over Text").guiText.enabled = false;
        //GameObject.Find("btn-pause").renderer.enabled = true;

		// Get screen borders
		float dist = (transform.position - Camera.main.transform.position).z;
		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;

		// Get the ground line gameObject
		player = GameObject.Find("Player");

		// Get height/width of obstacle & ground
		obsTileH = obsPrefab.GetComponent<Renderer>().bounds.size.y;
		obs2TileH = obs2Prefab.GetComponent<Renderer>().bounds.size.y;
		groundTileH = groundPrefab.GetComponent<Renderer>().bounds.size.y;

		obsCollider = obsPrefab.GetComponent<Collider2D>() as BoxCollider2D;
		obs2Collider = obs2Prefab.GetComponent<Collider2D>() as BoxCollider2D;

		// Get and position pause button
		//GameObject btnPause = GameObject.Find("btn-pause");
//		btnPause.transform.position = new Vector3(leftX + 0.2f, bottomY + btnPause.renderer.bounds.size.y/2 + 0, 0);

		// Gap between obstacles
		gap = 10;

		// Here we create 6 obstacles objects, so that they will loop in game.
		// We don't have to create new obstacles.

		obs = new GameObject[objectMax];
		GameObject obsRandom = getRandomObs();
        
		//障害物オブジェクトの出現位置設定（/2のところを1.5～1に変更）
        obs[0] = Instantiate(obsRandom, new Vector3(rightX + 5, bottomY + groundTileH / 1.6f + obsHeightScale, 0), Quaternion.identity) as GameObject;
        
        for(short idx = 1; idx < objectMax; idx++)
        {
            obsRandom = getRandomObs();
            obs[idx] = Instantiate(obsRandom, new Vector3(obs[idx - 1].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0), Quaternion.identity) as GameObject;
        }

		// Start Timed and Delayed functions to spawn obstacles/coins,
		// Check for game over, hide info after few seconds, etc
		StartCoroutine(changeSpeed());
		StartCoroutine(changeScore());
		StartCoroutine(showIfGameOver());
		StartCoroutine(hideInfo());

		// Set Game Over Music
		overSource = gameObject.AddComponent<AudioSource>();
		overSource.playOnAwake = false;
		overSource.rolloffMode = AudioRolloffMode.Linear;
		overSource.clip = overClip;

		// Set Game Dead Sound
		deadSource = gameObject.AddComponent<AudioSource>();
		deadSource.playOnAwake = false;
		deadSource.rolloffMode = AudioRolloffMode.Linear;
		deadSource.clip = deadClip;

		// If game is paused/over we have to set Timescale to 1 when Game starts
		if(Time.timeScale !=1){
			Time.timeScale = 1;
		}
	}


	GameObject getRandomObs(){
		GameObject obstacle;
		int rand = Random.Range(1, 6);
		if(rand <= 3){
			obstacle = obsPrefab;
			obsHeightScale = obsTileH/5;
			height1 = obsHeightScale;
		}else{
			obstacle = obs2Prefab;
            obsHeightScale = player.transform.GetComponent<Renderer>().bounds.size.y +player.transform.GetComponent<Renderer>().bounds.size.y / 30;
			height2 = obsHeightScale;
		}
		return obstacle;
	}

	// Method to change speed of game every 5 seconds
	IEnumerator changeSpeed() {
		while (true){
			yield return new WaitForSeconds(5);
			if(PlayerControl.speed > -4 && !PlayerControl.isDead){
				PlayerControl.speed -= 0.21f;
				gap -= 0.99f;
			}
		}
	}

	// Method to change score of game every 0.7 seconds
	IEnumerator changeScore() {
		while (true){
			yield return new WaitForSeconds(0.7f);
			if(!PlayerControl.isDead){

				Score.scoreNum += 1;
				if(Score.scoreNum >= 10000){//10000を超えたら
					NUM[0].SetActive(false);
					NUM[1].SetActive(false);
					scoreData.FinishScore();
					Score.scoreNum = 9999;//9999で固定
				}
			
				//スコアが9999以上になったら字幅を詰める
				if(Score.scoreNum > 9999 && !ScoreNumMoveFlag){
					//NUM[0].transform.Translate(-0.05f,0,0);
					NUM[0].GetComponent<Num_sprite>().width = 0.16f;
					ScoreNumMoveFlag = true;
				}

			}
		}
	}

	// Method to hide info texts after 3 Seconds
	IEnumerator hideInfo() {
		yield return new WaitForSeconds(3);
		//画像表示のためコメントアウト
		//GameObject.Find("Info Text").guiText.enabled = false;
		GameObject.Find ("InfoImage").SetActive(false);
	}

	// Method to check for game over every few seconds
	IEnumerator showIfGameOver() {
		while (true){
			yield return new WaitForSeconds(1.5f);
			if(PlayerControl.isDead){
				gameOverScene();
			}
		}
	}
	

	// Update is called once per frame
	void Update () {

		int topRandom = Random.Range (0, 2);
		int bottomRandom = Random.Range (0, 2);

		obsPrefab = bottomObjs [bottomRandom];
		obs2Prefab = topObjs[topRandom];

        //バナー広告があるか？

        if(banner == null)
        {
            banner = NendUtils.GetBannerComponent("nendAdBanner");
        }


        if (obs[0].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[0].GetComponent<ObsDownCollisionCheck>();
                //tc は 下の敵のとこを指している。　tc == null は GetComponentで取得したいコンポーネントを取得できなかった場合nullが入る
                if (tc == null)
                {
                    //nullなので下の敵を登録する
                    obs[0].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[0].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[0].GetComponent<ObsUpCollisionCheck>());
                    obs[0].AddComponent<ObsDownCollisionCheck>();
                }
                //ランダム生成の結果、前回と同じ(下の敵)オブジェクトだった為、下の敵用の高さに設定
                obsHeightScale = height1;
            }
            else
            {
                //oc は 上の敵のとこを指している。　
                ObsUpCollisionCheck oc = obs[0].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    //null の場合なので現在下の敵が入っている、今回上の敵を登録したいので今の下の敵を削除して上の敵を登録する
                    obs[0].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[0].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[0].GetComponent<ObsDownCollisionCheck>());
                    obs[0].AddComponent<ObsUpCollisionCheck>();
                }
                //上の適用の高さに設定する
                obsHeightScale = height2;
                if(obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            //0番目用の位置情報を設定する。
            obs[0].transform.position = new Vector3(obs[5].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[0].GetComponent<Collider2D>().enabled = true;
            obs[0].GetComponent<Renderer>().enabled = true;
        }
        if (obs[1].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[1].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[1].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[1].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[1].GetComponent<ObsUpCollisionCheck>());
                    obs[1].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[1].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[1].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[1].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[1].GetComponent<ObsDownCollisionCheck>());
                    obs[1].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[1].transform.position = new Vector3(obs[0].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[1].GetComponent<Collider2D>().enabled = true;
            obs[1].GetComponent<Renderer>().enabled = true;
        }
        if (obs[2].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[2].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[2].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[2].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[2].GetComponent<ObsUpCollisionCheck>());
                    obs[2].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[2].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[2].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[2].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[2].GetComponent<ObsDownCollisionCheck>());
                    obs[2].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[2].transform.position = new Vector3(obs[1].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[2].GetComponent<Collider2D>().enabled = true;
            obs[2].GetComponent<Renderer>().enabled = true;
        }
        if (obs[3].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[3].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[3].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[3].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[3].GetComponent<ObsUpCollisionCheck>());
                    obs[3].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[3].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[3].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[3].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[3].GetComponent<ObsDownCollisionCheck>());
                    obs[3].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[3].transform.position = new Vector3(obs[2].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[3].GetComponent<Collider2D>().enabled = true;
            obs[3].GetComponent<Renderer>().enabled = true;
        }
        if (obs[4].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[4].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[4].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[4].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[4].GetComponent<ObsUpCollisionCheck>());
                    obs[4].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[4].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[4].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[4].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[4].GetComponent<ObsDownCollisionCheck>());
                    obs[4].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[4].transform.position = new Vector3(obs[3].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[4].GetComponent<Collider2D>().enabled = true;
            obs[4].GetComponent<Renderer>().enabled = true;
        }
        if (obs[5].transform.position.x + obsPrefab.GetComponent<Renderer>().bounds.size.x < leftX)
        {
            int rand = Random.Range(1, 6);
            if (rand <= 3)
            {
                ObsDownCollisionCheck tc = obs[5].GetComponent<ObsDownCollisionCheck>();
                if (tc == null)
                {
                    obs[5].GetComponent<SpriteRenderer>().sprite = obsPrefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[5].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obsCollider.size;
                    Destroy(obs[5].GetComponent<ObsUpCollisionCheck>());
                    obs[5].AddComponent<ObsDownCollisionCheck>();
                }
                obsHeightScale = height1;
            }
            else
            {
                ObsUpCollisionCheck oc = obs[5].GetComponent<ObsUpCollisionCheck>();
                if (oc == null)
                {
                    obs[5].GetComponent<SpriteRenderer>().sprite = obs2Prefab.GetComponent<SpriteRenderer>().sprite;
                    BoxCollider2D b = obs[5].GetComponent<Collider2D>() as BoxCollider2D;
                    b.size = obs2Collider.size;
                    Destroy(obs[5].GetComponent<ObsDownCollisionCheck>());
                    obs[5].AddComponent<ObsUpCollisionCheck>();
                }
                obsHeightScale = height2;
                if (obsHeightScale < 0.0f)
                {
                    obsHeightScale = 1.11600018f;
                }
            }
            obs[5].transform.position = new Vector3(obs[4].transform.position.x + RandomGap(), bottomY + groundTileH / 1.6f + obsHeightScale, 0);
            obs[5].GetComponent<Collider2D>().enabled = true;
            obs[5].GetComponent<Renderer>().enabled = true;
        }

		// When back button is pressed, if its not paused, we pause the game,
		// If game is paused, we resume the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused && !PlayerControl.isDead)
            {
                Time.timeScale = 0; // pause
                paused = true;
                // Make paused screen elements visible when paused
                pauseBg.enabled = true;
                btnResume.enabled = true;
                btnHome.enabled = true;
                btnReplay.enabled = true;
               // pauseText.enabled = true;
                PlayerControl.musicSource.Stop(); // stop music


            }
            else if (paused && !PlayerControl.isDead)
            {
                Time.timeScale = 1; // resume
                paused = false;
                // Hide paused screen elements when resumed
                pauseBg.enabled = false;
                btnResume.enabled = false;
                btnHome.enabled = false;
                btnReplay.enabled = false;
               // pauseText.enabled = false;
                if (MenuScreen.isSoundOn)
                {
                    PlayerControl.musicSource.Play(); // resume music
                }
            }
        }
	}

	// Method to show Game Over Screen
	public void gameOverScene(){
		PlayerControl.isDead = true;
        NendAdInterstitial.Instance.Show();
		// If Current score if better than previously saved score, store current score
		if(Score.scoreNum > Score.bestNum){
			if(Score.scoreNum<=9999){
			PlayerPrefs.SetInt("Score", Score.scoreNum);
			}
		}
		Time.timeScale = 0; // Stop game
		// Show Game Over screen elements
        SherePlugins.SetActive(true);
		overBg.enabled = true;
		overScoreObs.SetActive (true);
		btnHomeOver.enabled = true;
		btnReplayOver.enabled = true;
		btnPause.SetActive (false);
		PlayerControl.musicSource.Stop(); // stop music
		if(MenuScreen.isSoundOn){
			overSource.Play(); // play game over sound
		}



        //banner.Show();
	}


    private int RandomGap()
    {
        int gap = 10;
        if(Score.scoreNum > 40 && Score.scoreNum < 70)
        {
            gap = Random.Range(5, 9);
        }
        else if(Score.scoreNum > 70)
        {
            gap = Random.Range(3, 7);
        }
        return gap;
    }
}



