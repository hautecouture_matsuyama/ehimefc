using UnityEngine;
using System;
using System.Collections.Generic;
using SimpleJSON;

namespace I2.Loc
{
	public partial class LanguageSource
	{
		public string Import_CSV( string Category, string CSVstring, eSpreadsheetUpdateMode UpdateMode = eSpreadsheetUpdateMode.Replace, char Separator = ',' )
		{
			List<string[]> CSV = LocalizationReader.ReadCSV (CSVstring, Separator);
			
			//--[ Languages ]--------------
			
			string[] Tokens = CSV[0];

			int LanguagesStartIdx = 1;
			int TypeColumnIdx = -1;
			int DescColumnIdx = -1;

			var ValidColumnName_Key  = new string[]{ "Key" };
			var ValidColumnName_Type = new string[]{ "Type" };
			var ValidColumnName_Desc = new string[]{ "Desc", "Description" };

			if (Tokens.Length>1 && ArrayContains(Tokens[0], ValidColumnName_Key))
			{
				if (UpdateMode == eSpreadsheetUpdateMode.Replace)
					ClearAllData();

				if (Tokens.Length>2)
				{
					if (ArrayContains(Tokens[1], ValidColumnName_Type)) 
					{
						TypeColumnIdx = 1;
						LanguagesStartIdx = 2;
					}
					if (ArrayContains(Tokens[1], ValidColumnName_Desc)) 
					{
						DescColumnIdx = 1;
						LanguagesStartIdx = 2;
					}

				}
				if (Tokens.Length>3)
				{
					if (ArrayContains(Tokens[2], ValidColumnName_Type)) 
					{
						TypeColumnIdx = 2;
						LanguagesStartIdx = 3;
					}
					if (ArrayContains(Tokens[2], ValidColumnName_Desc)) 
					{
						DescColumnIdx = 2;
						LanguagesStartIdx = 3;
					}
				}
			}
			else
				return "Bad Spreadsheet Format.\nFirst columns should be 'Key', 'Type' and 'Desc'";

			int nLanguages = Mathf.Max (Tokens.Length-LanguagesStartIdx, 0);
			int[] LanIndices = new int[nLanguages];
			for (int i=0; i<nLanguages; ++i)
			{
				string LanName, LanCode;
				GoogleLanguages.UnPackCodeFromLanguageName(Tokens[i+LanguagesStartIdx], out LanName, out LanCode);

				int LanIdx = -1;
				if (!string.IsNullOrEmpty(LanCode))
					LanIdx = GetLanguageIndexFromCode(LanCode);
				else
					LanIdx = GetLanguageIndex(LanName);

				if (LanIdx < 0)
				{
					LanguageData lanData = new LanguageData();
					lanData.Name = LanName;
					lanData.Code = LanCode;
					mLanguages.Add (lanData);
					LanIdx = mLanguages.Count-1;
				}
				LanIndices[i] = LanIdx;
			}

			//--[ Update the Languages array in the existing terms]-----
			nLanguages = mLanguages.Count;
			for (int i=0, imax=mTerms.Count; i<imax; ++i)
			{
				TermData termData = mTerms[i];
				if (termData.Languages.Length < nLanguages)
					Array.Resize( ref termData.Languages, nLanguages );
			}
			
			//--[ Keys ]--------------

			for (int i=1, imax=CSV.Count; i<imax; ++i)
			{
				Tokens = CSV[i];
				string sKey = string.IsNullOrEmpty (Category) ? Tokens[0] : string.Concat( Category, "/", Tokens[0]);
				LanguageSource.ValidateFullTerm(ref sKey);

				TermData termData = GetTermData(sKey);

				// Check to see if its a new term
				if (termData==null)
				{
					termData = new TermData();
					termData.Term = sKey;

					termData.Languages = new string[ mLanguages.Count ];
					for (int j=0; j<mLanguages.Count; ++j) 
						termData.Languages[j] = string.Empty;

					mTerms.Add (termData);
					mDictionary.Add (sKey, termData);
				}
				else
				// This term already exist
				if (UpdateMode==eSpreadsheetUpdateMode.AddNewTerms)
					continue;

				if (TypeColumnIdx>0)
					termData.TermType = GetTermType(Tokens[TypeColumnIdx]);

				if (DescColumnIdx>0)
					termData.Description = Tokens[DescColumnIdx];

				for (int j=0; j<LanIndices.Length && j<Tokens.Length-LanguagesStartIdx; ++j)
					if (!string.IsNullOrEmpty(Tokens[j+LanguagesStartIdx]))	// Only change the translation if there is a new value
						termData.Languages[ LanIndices[j] ] = Tokens[j+LanguagesStartIdx];
			}

			return string.Empty;
		}

		bool ArrayContains( string MainText, params string[] texts )
		{
			for (int i=0, imax=texts.Length; i<imax; ++i)
				if (MainText.IndexOf(texts[i], StringComparison.OrdinalIgnoreCase)>=0)
					return true;
			return false;
		}

		public static eTermType GetTermType( string type )
		{
			for (int i=0, imax=(int)eTermType.Object; i<=imax; ++i)
				if (string.Equals( ((eTermType)i).ToString(), type, StringComparison.OrdinalIgnoreCase))
					return (eTermType)i;
			
			return eTermType.Text;
		}
	}
}