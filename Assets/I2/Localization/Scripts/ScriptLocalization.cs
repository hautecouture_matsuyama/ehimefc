﻿// This class is Auto-Generated by the Script Tool in the Language Source
using UnityEngine;

namespace I2.Loc
{
	public static class ScriptLocalization
	{
		public static string Get( string Term, bool FixForRTL = false ) { return LocalizationManager.GetTermTranslation(Term, FixForRTL); }
	}
}
