﻿using System; 
using UnityEngine; 
using System.Collections; 
using UnityEngine.UI; 

public class StopTimerScript : MonoBehaviour { 

	//時間、分、秒
	[SerializeField] 
	Image[] images = new Image[6]; 

	//0～9の画像
	[SerializeField] 
	Sprite[] numberSprites = new Sprite[10]; 

	[SerializeField]
	int debugtime=0;
	
	public float timeCount { get; private set; } 
	
	void Start() 
	{ 
		SetTime(debugtime); //時間を設定（900なら15分
	} 

	//時間設定関数
	public void SetTime(float time) 
	{ 
		//引数から時間の値を受け取る
		timeCount = time; 
		//タイマー開始
		StartCoroutine(TimerStart()); 
	} 

	//時間、分、秒の各ｓｐｒｉｔｅに配列の中身を適用
	void SetNumbers(int sec, int val1, int val2)//val1:秒　val2,分
	{ 
		//string str = String.Format("{0:00}:{1:00}:{2:00}:{3:00}", Math.Floor(timeCount / 3600f), Math.Floor(timeCount / 60f), Math.Floor(timeCount % 60f), timeCount % 1 * 100);
		string str = String.Format("{0:00}",sec); 
		//スプライトの設定
		images[val1].sprite = numberSprites[Convert.ToInt32(str.Substring(0,1))]; 
		images[val2].sprite = numberSprites[Convert.ToInt32(str.Substring(1,1))]; 
	} 
	
	IEnumerator TimerStart() 
	{ 
		while (timeCount >= 0) { 
			int sec = Mathf.FloorToInt(timeCount % 60); //秒数の計算
			SetNumbers(sec, 2, 3); //imagesの配列2,3番目を適用
			int minu = Mathf.FloorToInt(timeCount/ 60); //分数の計算
			//Debug.Log(minu);
			SetNumbers(minu, 0, 1); //imagesの配列0,1番目を適用
			int hour = Mathf.FloorToInt(timeCount/ 3600f);//時間の計算
			SetNumbers(hour, 4, 5); //imagesの配列0,1番目を適用
			yield return new WaitForSeconds(1.0f); //一秒待つ
			//上記一秒待ったら1秒加算
			timeCount += 1.0f; 
			if(minu >=60){
				minu = 0;
			}
		} 
		//TimeOver(); 
	} 
	
	/*void TimeOver() 
	{ 
		TextMesh text = new GameObject().AddComponent<TextMesh>(); 
		text.text = "Time Over!";         
	} */
} 
