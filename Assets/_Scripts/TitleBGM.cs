﻿using UnityEngine;
using System.Collections;

public class TitleBGM : MonoBehaviour {
	public AudioSource musicSource;
	public AudioClip musicClip;
	// Use this for initialization
	void Start () {
		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.playOnAwake = false;
		musicSource.volume = 0.55f;
		musicSource.loop = true;
		musicSource.rolloffMode = AudioRolloffMode.Linear;
		musicSource.clip = musicClip;
		if (MenuScreen.isSoundOn) {
			musicSource.Play ();
		}


	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
