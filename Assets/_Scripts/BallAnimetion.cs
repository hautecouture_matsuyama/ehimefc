﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public enum BalltState{
	DRIBBLE =0,
	SLIDING = 1,
	BALLJUMP=2
}

public class BallAnimetion : MonoBehaviour {

	[SerializeField]
	Transform[] target = new Transform[1];
	[SerializeField]
  float EASING = 0.1f;
	
	public BalltState BallAnimeState;

	public short rotateSpeed;
	Vector3 diff;

	int posNo=0;
	// Use this for initialization
	void Start () {
		rotateSpeed = -800;
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerControl.isDead || GameScreen.paused) {
			EASING = 0;
		} else {
			EASING = 6f;
			transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime));
		}


		if (BallAnimeState == BalltState.DRIBBLE && posNo<=1) {
			Dribble ();
		}

		if (BallAnimeState == BalltState.SLIDING) {
			Sliding ();
		}

		if (BallAnimeState == BalltState.BALLJUMP) {

			BallJump ();
		}


	}

	void Dribble(){
			diff = target [posNo].position - this.transform.position;
			if( diff.magnitude < 0.1f ) 
			{
				posNo++;
				if(posNo >1){
					posNo = 0;
				}
			}
		Vector3 v = diff * EASING *Time.deltaTime;
			this.transform.position += v;
	}

	void Sliding (){
		diff = target [2].position - this.transform.position;
		if( diff.magnitude < 0.1f ) 
		{
			posNo = 0;
			BallAnimeState = BalltState.DRIBBLE;
		}
		Vector3 v = diff * EASING *Time.deltaTime;
		this.transform.position += v;
	}
	
	void BallJump(){
		diff = target [3].position - this.transform.position;
		if( diff.magnitude < 0.1f ) 
		{
			posNo = 0;
			BallAnimeState = BalltState.DRIBBLE;
		}
		Vector3 v = diff * EASING *Time.deltaTime;
		this.transform.position += v;
		 
	}
}
