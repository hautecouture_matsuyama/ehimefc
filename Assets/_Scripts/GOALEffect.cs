﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GOALEffect : MonoBehaviour {
	float timer =0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float A;
		timer += Time.deltaTime;
		A = Mathf.Sin(timer*3.0f)+2.5f;
		this.gameObject.transform.localScale = new Vector3 (A/2.5f, A/2.5f, A);
	}
}
